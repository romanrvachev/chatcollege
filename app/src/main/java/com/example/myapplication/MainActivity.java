package com.example.myapplication;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private SharedPreferences prefs;
    private static final String prefName = "myPrefs";
    private String currentName = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        edit2=findViewById(R.id.edit2);
        text1=findViewById(R.id.textView);

        prefs = getSharedPreferences(prefName, Context.MODE_PRIVATE);
        if(prefs.contains("Name")){
            currentName = prefs.getString("Name", "");
        }

        DatabaseReference myRef = database.getReference();

        DatabaseReference chatRef = myRef.child("Chat");

        chatRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                text1.setText("");
                ArrayList<String> keys = new ArrayList<>();
                for(DataSnapshot ds : dataSnapshot.getChildren()){
                    keys.add(ds.getKey());
                    text1.setText(text1.getText().toString() + "\n" + ds.getValue());
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Toast.makeText(MainActivity.this, "Error!", Toast.LENGTH_LONG).show();
            }
        });
    }
    EditText edit2;
    TextView text1;
    FirebaseDatabase database= FirebaseDatabase.getInstance();
    public void setClick(View view) {
        DatabaseReference myRef = database.getReference("Chat");
        myRef.setValue(currentName + ": " + edit2.getText().toString());
    }

    public void logOut(View view){
        SharedPreferences.Editor editor = prefs.edit();
        editor.remove("Name");
        editor.apply();
        startActivity(new Intent(MainActivity.this, LoginActivity.class));
    }
}