package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class LoginActivity extends AppCompatActivity {

    private SharedPreferences prefs;
    private static final String prefName = "myPrefs";
    private EditText userNameText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        userNameText = findViewById(R.id.userName);

        prefs = getSharedPreferences(prefName, Context.MODE_PRIVATE);
        if(prefs.contains("Name")){
            startActivity(new Intent(LoginActivity.this, MainActivity.class));
        }
    }

    public void onClick(View view){
        if(userNameText.getText().toString().length() >= 4){
            SharedPreferences.Editor editor = prefs.edit();

            editor.putString("Name", userNameText.getText().toString());
            editor.apply();
            startActivity(new Intent(LoginActivity.this, MainActivity.class));
        }else{
            Toast.makeText(LoginActivity.this, "Имя пользователя должно содержать не менее 4 символов!", Toast.LENGTH_SHORT).show();
        }
    }
}